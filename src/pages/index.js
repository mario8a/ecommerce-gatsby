import * as React from "react"
import { graphql } from "gatsby";
import Seo from "../components/seo"
import { Jumbo } from './../components/Jumbo';
import { Products } from './../components/Products';

export const query = graphql`
query  GET_DATA{
  allSite{
    edges {
      node {
        siteMetadata {
          description
        }
      }
    }
  }
  allStripePrice {
    edges {
      node {
        id
        unit_amount
        product {
          name
          description
          images
        }
      }
    }
  }
}
`

const IndexPage = ({data}) =>{ 
  console.log(data)
  return (
  <>
    <Seo title="Home" />
    <Jumbo description={data.allSite.edges[0].node.siteMetadata.description}/>
    <Products products={data.allStripePrice.edges} />
  </>
)}

export default IndexPage
