import * as React from "react"
import PropTypes from "prop-types"
import {Content} from '../styles/components';

import Header from "./header"
import "./layout.css"

const Layout = ({ children }) => {

  return (
    <>
      <Header/>
      <Content>
        <main>{children}</main>
        {/* <Footer>
          con 💙
          <a href="https://mario8a.netlify.app/">Mario</a>
        </Footer> */}
      </Content>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
