import React from 'react';
import { Link } from 'gatsby';
import priceFormat from './../utils/priceFormat';
import { StyledProducts } from '../styles/components';

export const Products = ({products}) => {
   return (
      <StyledProducts>
         <h2>Productos</h2>
         <section>
            { products.map(({node}) => {
               const price =  priceFormat(node.unit_amount)
               return (
                  <article key={node.id}>
                     <img src={node.product.images[0]} alt={node.product.name} />
                     <p> {node.product.name} </p>
                     <small> MX {price}</small>
                     <Link to={`/${node.id}`}>
                        Comprar ahora
                     </Link>
                  </article>
               )
            }) }
         </section>
      </StyledProducts>
   )
}
