import React, { useState, useContext } from 'react';
import priceFormat from './../utils/priceFormat';
import { CartContext } from '../context';
import Seo from './seo';
import {
   Tag,
   Button,
   StyledProductDetail,
   QtySelect
} from '../styles/components';
import { Stars } from './Stars';

export const ProductDetail = ({id, unit_amount, product}) => {
   const { name } = product;
   const formatPrice = priceFormat(unit_amount);
   const [qty, setQty] = useState(1);
   const { cart, addToCart } = useContext(CartContext);

   const handleSubmit = () => {
      console.log('He');
      addToCart({unit_amount, id, name, quantity: qty, product});
    }
  
   
   return (
      <StyledProductDetail>
         <Seo title={product.name}/>
         <img src={product.images[0]} alt={product.name} />
         <div>
            <Tag>Popular</Tag>
            <h2> {product.name} </h2>
            <b> MX {formatPrice} </b>
            <Stars />
            <small> {product.description} </small>
            <QtySelect>
               <button onClick={() => (qty > 1 ? setQty(qty-1) : null)}>
                  -
               </button>
               <input type="text" disabled value={qty}/>
               <button onClick={() => setQty(qty+1)}> + </button>
            </QtySelect>
            <Button onClick={handleSubmit}>Agregar al carrito</Button>
         </div>
      </StyledProductDetail>
   )
}
