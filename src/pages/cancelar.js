import { Link } from 'gatsby';
import React from 'react'
import { Button, Purchase } from '../styles/components';
import Seo from './../components/seo';

export default function cancelar() {
   return (
      <div>
         <Seo title='Compra cancelada' />
         <Purchase>
            <h2>Compra Cancelada</h2>
            <p>
               Sentimos que no hayas comprado tu nuevo producto.
            </p>
            <p>
               Reacuerda que aqui seguiran por si decides volver
            </p>
            <p> Te esperamos de vuelta! </p>
            <span role='img' aria-label='emoji'>💚</span>
            <Link to="/">
               <Button>Volver al Catalogo</Button>
            </Link>
         </Purchase>
      </div>
   )
}
