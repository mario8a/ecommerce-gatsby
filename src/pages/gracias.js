import React from 'react';
import { Link } from "gatsby"
import { Button, Purchase } from '../styles/components';
import Seo from './../components/seo';

const gracias = () => {
   return (
      <div>
         <Seo title='Compra exitosa' />
         <Purchase>
            <h2>Compra exitosa</h2>
            <p>Espero que disfrutes tu swag, lucelo con orgullo</p>
            <p>!No pares de aprender</p>
            <span role='img' aria-label='emoji'>💚</span>
            <Link to="/">
               <Button>Volver al Catalogo</Button>
            </Link>
         </Purchase>
      </div>
   )
}

export default gracias